# QuickBase Query Language (qbql)
QuickBase API using standard SQL syntax

## Getting Started
Install from npm:
```
$ npm i qbql
```
Or get the source code here:
```
$ git remote add origin https://davidjbarnes@bitbucket.org/davidjbarnes/qbql.git
```

## Usage
```
import QBQL from 'qbql';

qbql = new QBQL({
    domain: <DOMAIN>,
    database: <DATABASE>,
    username: <USERNAME>,
    password: <PASSWORD>,
    onReady: (response) => {
        console.log(response);
    },
    onError: (error) => {
        console.log(error);
    }
});
```

## Methods
### Select
```
qbql.query("select name from user").then((response) => {
    console.log(response);
});
```

### Where
```
qbql.query("select name from user where name = 'David'").then((response) => {
    console.log(response);
});
```

#### Operators
Valid where operators:
* '='  Is equal to a specific value
* '!=' Is not equal to a specific value
* '<'  Is less than a specific value
* '<=' Is less than or equal to a specific value
* '>'  Is greater than a specific value
* '>=' Is greater than or equal to a specific value

## Work in Progress
* Insert (standard)
* Bulk insert
* Stored Procs
* Update
* Delete
* Join
* Aggregate functions

## Contributing
Email me: NullableInt@gmail.com 

## Authors

* **David J Barnes** - *NullableInt@gmail.com*

## License
This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
// @flow
import axios from 'axios';
import XmlConvert from './utilities/xml-conversion';

const xmlConvert = new XmlConvert();
const parse = require('node-sqlparser').parse;

class Index {
    constructor(config: {domain: String, database: String, username: String, password: String, onReady: _ => void, onError: _ => void}) {
        this.domain = config.domain || "quickbase.com";
        this.database = config.database || "database";
        this.username = config.username || "jeffy";
        this.password = config.password || "abc123";

        this.databaseSchema = null;
        this.tableSchemas = [];

        this.init(config.onReady, config.onError);
    }

    init(onReady, onError): void {
        Promise.resolve()
            .then(() => this.auth())
            .then(() => this.cacheDatabaseSchema())
            .then(() => this.cacheTableSchemas())
            .then(() => onReady({ready:true, databaseSchema: this.databaseSchema, tableSchemas: this.tableSchemas}))
            .catch(error => {
                onError(error);
            });
    }

    cacheDatabaseSchema(): Promise {
        return new Promise((resolve, reject) => {
            this.getSchema(this.database)
                .then(databaseSchema => {
                    this.databaseSchema = databaseSchema.table; resolve(true);
                })
                .catch(error => { reject(error) });
        });
    }

    cacheTableSchemas(): Promise {
        let promises = [];

        this.databaseSchema.chdbids.chdbid.map((entity) => {
            promises.push(this.getSchema(entity.text));
        });

        return Promise.all(promises).then((values) => {
            values.map((schema) => {
                this.tableSchemas.push(schema.table);
            })
        });
    }

    auth(): Promise {
        return new Promise((resolve, reject) => {
            const url = "https://".concat(this.domain,
                "/db/main?a=API_Authenticate&username=", this.username,
                "&password=", this.password,
                "&hours=24");

            axios.get(url).then((response) => {
                const responseJson = xmlConvert.xmlToJson(response.data);

                if (responseJson.errcode.text === 0) {
                    this.ticket = responseJson.ticket.text;
                    resolve(responseJson);
                } else {
                    reject(responseJson);
                }
            });
        });
    }

    getSchema(schemaName: String): Promise {
        return new Promise((resolve, reject) => {
            const url = "https://".concat(this.domain,
                "/db/", schemaName, //may be a table name OR database name
                "?a=API_GetSchema",
                "&ticket=", this.ticket);

            axios.get(url).then((response) => {
                const responseJson = xmlConvert.xmlToJson(response.data);

                if (responseJson.errcode.text === 0) {
                    resolve(responseJson);
                } else {
                    reject(responseJson);
                }
            });
        });
    }

    getFieldIdsFromColumnNames(astObj: Object): String {
        let fids = [];
        const tableName = astObj.from[0].table;

        const tableSchema = this.tableSchemas.find((t) => {
            return t.name.text === tableName;
        });

        if (astObj.columns !== "*") {
            astObj.columns.map((column) => {
                fids.push(tableSchema.fields.field.find((f) => {
                    return f.label.text === column.expr.column
                })._attributes.id);
            });

            return "&clist=".concat(fids.toString().replace(",", "."));
        } else {
            return null;
        }
    }

    getQBTableNameFromSQL(tableName: String): String {
        return this.tableSchemas.find((o) => {
            return o.name.text === tableName;
        });
    }

    getWhereClause(astObj: Object) {
        //<query>{4.EX.’Jeanne_Smith@acme.com’}</query>
        // EX - Is equal to either a specific value, or the value in another field of the same type.
        // XEX - Is not equal to either a specific value, or the value in another field of the same field type.

        // LT - Is less than either a specific value or the value in another field of the same type.
        // LTE - Is less than or equal to either a specific value or the value in another field of the same type.
        // GT - Is greater than either a specific value or the value in another field of the same type.
        // GTE - Is greater than or equal to either a specific value or the value in another field of the same type.

        const where = astObj.where;

        if(where) {
            const operators = [
                {qbOp: 'EX', sqlOp: '='},
                {qbOp: 'XEX', sqlOp: '!='},
                {qbOp: 'LT', sqlOp: '<'},
                {qbOp: 'LTE', sqlOp: '<='},
                {qbOp: 'GT', sqlOp: '>'},
                {qbOp: 'GTE', sqlOp: '>='}
            ];

            const qbOp = operators.find((o) => {
                return o.sqlOp === where.operator;
            }).qbOp;

            return "&query={%27name%27." + qbOp + ".%27" + where.right.value + "%27}";
        } else {
            return "";
        }
    }

    query(sql: String): Promise {
        return new Promise((resolve, reject) => {
            const astObj = parse(sql);
            const tableName = astObj.from[0].table;
            // console.log("astObj:", astObj);

            const qbTableName = this.getQBTableNameFromSQL(tableName).original.table_id.text;
            // console.log("qbTableName:", qbTableName);

            const clist = this.getFieldIdsFromColumnNames(astObj);
            // console.log(clist);

            const where = this.getWhereClause(astObj);
            // console.log(where);

            const url = "https://" + this.domain + "/db/" + qbTableName + "?a=API_DoQuery&includeRids=1&ticket=" + this.ticket + where + clist;
            //console.log("url: ", url);

            axios.get(url).then((response) => {
                const responseJson = xmlConvert.xmlToJson(response.data);

                if (responseJson.errcode.text === 0) {
                    resolve(responseJson);
                } else {
                    reject(responseJson);
                }
            });
        });
    }

    proc(name: String): Object {

    }
}

export default Index;
const convert = require('xml-js');

const nativeType = (value) => {
    const nValue = Number(value);
    if (!isNaN(nValue)) {
        return nValue;
    }
    const bValue = value.toLowerCase();
    if (bValue === 'true') {
        return true;
    } else if (bValue === 'false') {
        return false;
    }
    return value;
}

const removeJsonTextAttribute = (value, parentElement) => {
    try {
        const keyNo = Object.keys(parentElement._parent).length;
        const keyName = Object.keys(parentElement._parent)[keyNo - 1];
        parentElement._parent[keyName] = nativeType(value);
    } catch (e) {
    }
}

const xml2jsonOptions = {
    compact: true,
    trim: true,
    ignoreDeclaration: true,
    ignoreInstruction: true,
    ignoreAttributes: false,
    ignoreComment: true,
    ignoreCdata: true,
    ignoreDoctype: true,
    nativeType: true,
    textKey: "text"
    //textFn: removeJsonTextAttribute
};

class XmlConvert {
    xmlToJson(data) {
        return JSON.parse(convert.xml2json(data, xml2jsonOptions)).qdbapi
    }
}

export default XmlConvert;